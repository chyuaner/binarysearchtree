#include <iostream>
#include <cstdlib>
using namespace std;

class BinarySearchTree_Node{
	friend class BinarySearchTree;
	private:
		int number;
		BinarySearchTree_Node *leftChild, *rightChild, *parent;
	public:
		BinarySearchTree_Node(int inputNum){
			setNumber(inputNum);
			parent = NULL;
			leftChild = NULL;
			rightChild = NULL;
		}
		int getNumber(){
			return number;
		}
		void setNumber(int inputNum){
			number = inputNum;
		}
};

class BinarySearchTree{
	private:
		int nodeTotal;	//目前共有幾個節點
		BinarySearchTree_Node *firstNode;
		
		//搜尋這個數的節點
		BinarySearchTree_Node *searchNodeObject(int num){
			//如果根節點尚未建立的話
			if(firstNode == NULL){
				return NULL;	//無節點，回傳NULL
			}
			//在已經有節點的情況下
			else{
				BinarySearchTree_Node *currentNode = firstNode;	//當前節點
				while(true){
					//若這個節點的數字比目前節點的數字大
					if(num > currentNode->number){
						//如果還能往右兒子找的話
						if(currentNode->rightChild != NULL){
							currentNode = currentNode->rightChild;	//節點指標往右找
						}
						//如果已經是最底下兒子的話
						else{
							return NULL;	//找不到，回傳NULL
						}
					}
					//若這個節點的數字比目前節點的數字小
					else if(num < currentNode->number){
						//如果還能往左兒子找的話
						if(currentNode->leftChild != NULL){
							currentNode = currentNode->leftChild;	//節點指標往左找
						}
						//如果已經是最底下兒子的話
						else{
							return NULL;	//找不到，回傳NULL
						}
					}
					//這個節點的數字與目前節點的數字相同
					else{
						return currentNode;	//找到節點，回傳節點物件
					}
				}
			}
		}
		//印出節點資訊
		void printObjectNodeInfo(BinarySearchTree_Node *thisNode){
			if(thisNode != NULL){
				cout<<thisNode->getNumber()<<":";
				
				cout<<"\t";
				cout<<"leftChild: ";
				if(thisNode->leftChild != NULL)
					cout<<thisNode->leftChild->getNumber();
				else cout<<"--";
				
				cout<<"\t";
				cout<<"rightChild: ";
				if(thisNode->rightChild != NULL)
					cout<<thisNode->rightChild->getNumber();
				else cout<<"--";
				
				cout<<"\t";
				cout<<"parent: ";
				if(thisNode->parent != NULL)
					cout<<thisNode->parent->getNumber();
				else cout<<"--";
				
				cout<<endl;
			}
			else{
				cout<<"No Found!"<<endl;
			}
		}
		void printOrderTextHelper(BinarySearchTree_Node *thisNode, int choose){
			switch(choose){
			case 1:	//列出節點內容
				cout<<thisNode->getNumber()<<" -> ";
				break;
			case 2:	//列出節點資訊
				printObjectNodeInfo(thisNode);
				break;
			}
		}
		//遞迴 列印Inorder節點資訊
		void printInorderListHelper(BinarySearchTree_Node *thisNode, int choose){
			if(thisNode != NULL){
				if(thisNode->leftChild != NULL) printInorderListHelper(thisNode->leftChild, choose);
				printOrderTextHelper(thisNode, choose);
				printInorderListHelper(thisNode->rightChild, choose);
			}
		}
		//遞迴 列印Preorder節點資訊
		void printPreorderListHelper(BinarySearchTree_Node *thisNode, int choose){
			if(thisNode != NULL){
				printOrderTextHelper(thisNode, choose);
				if(thisNode->leftChild != NULL) printPreorderListHelper(thisNode->leftChild, choose);
				printPreorderListHelper(thisNode->rightChild, choose);
			}
		}
		
	public:
		BinarySearchTree(){
			nodeTotal = 0;
			firstNode = NULL;
		}
		
		//是否為空
		bool isEmpty(){
			if(firstNode == NULL) return true;
			else return false;
		}
		
		//取得共有幾個節點
		int getNodeTotal(){
			return nodeTotal;
		}
		
		//建立
		bool insertNode(int num){
			BinarySearchTree_Node *node = new BinarySearchTree_Node(num);	//新增一個節點
			
			//如果根節點尚未建立的話
			if(firstNode == NULL){
				firstNode = node;	//指定根節點為這個節點
				nodeTotal = 1;	//節點總計為1
				return true;	//回傳成功狀態
			}
			//在已經有節點的情況下
			else{
				BinarySearchTree_Node *currentNode = firstNode;	//當前節點指標
				int currentLevel = 0;	//當前層級數
				while(1){
					//若這個節點的數字比目前節點的數字大
					if(num > currentNode->number){
						//如果還能往右兒子找的話
						if(currentNode->rightChild != NULL){
							currentNode = currentNode->rightChild;	//節點指標往右兒子找
							currentLevel++;	//當前層級數+1
						}
						//如果已經是最底下兒子的話
						else{
							//新增這個節點
							currentNode->rightChild = node;	//指定右兒子為這個node
							node->parent = currentNode;	//指定這個node的父親為當前節點
							nodeTotal++;	//節點總計+1
							return true;	//回傳成功狀態
						}
					}
					//若這個節點的數字比目前節點的數字小
					else if(num < currentNode->number){
						//如果還能往左兒子找的話
						if(currentNode->leftChild != NULL){
							currentNode = currentNode->leftChild;	//節點指標往左兒子找
							currentLevel++;	//當前層級數+1
						}
						//如果已經是最底下兒子的話
						else{
							//新增這個節點
							currentNode->leftChild = node;	//指定左兒子為這個node
							node->parent = currentNode;	//指定這個node的父親為當前節點
							nodeTotal++;	//節點總計+1
							return true;	//回傳成功狀態
						}
					}
					//這個節點的數字與目前節點的數字相同
					else{
						return false;	//回傳失敗狀態
					}
				}
			}
		}
		
		//刪除其中節點
		bool deleteNode(int num){
			BinarySearchTree_Node *thisNode = searchNodeObject(num);	//尋找這個數的節點代入thisNode
			
			//如果有搜尋到此數字的話
			if(thisNode != NULL){
				nodeTotal--;	//節點總計-1
				BinarySearchTree_Node *currentNode = thisNode;	//當前節點指標
				//如果這個節點有右兒子
				if(thisNode->rightChild != NULL){
					currentNode = thisNode->rightChild;	//節點指標往右兒子找
				}
				while(true){
					//如果這個節點有左兒子
					if(currentNode->leftChild != NULL){
						currentNode = currentNode->leftChild;	//節點指標往左兒子找
					}
					//如果已經是最底下兒子的話
					else{
						//如果舊節點的左兒子不是自己節點，且舊節點的左兒子非無節點的話
						if(thisNode->leftChild != currentNode && thisNode->leftChild != NULL){
							if(thisNode != currentNode) currentNode->leftChild = thisNode->leftChild;	//將這個節點與準備要被取代節點的左兒子連接
							else currentNode->leftChild = NULL;
							if(currentNode->parent != NULL){
								if(thisNode->parent != NULL) currentNode->parent->leftChild = NULL;	//將這個節點的父親節點脫離關係
							}
							thisNode->leftChild->parent = currentNode;	//將此節點左兒子的父親指定為這個節點
						}
						//如果舊節點的左兒子是自己節點的話
						else{
							if(thisNode->parent == NULL) currentNode->parent = NULL;	//避免對自己節點接到
							else currentNode->parent = thisNode->parent;
						} 
						
						//如果舊節點的右兒子不是自己節點，且舊節點的右兒子非無節點的話
						if(thisNode->rightChild != currentNode && thisNode->rightChild != NULL){
							if(thisNode != currentNode) currentNode->rightChild = thisNode->rightChild;	//將這個節點與準備要被取代節點的右兒子連接
							else currentNode->rightChild = NULL;
							if(currentNode->parent != NULL){
								if(thisNode->parent != NULL) currentNode->parent->rightChild = NULL;	//將這個節點的父親節點脫離關係
							}
							thisNode->rightChild->parent = currentNode;	//將此節點右兒子的父親指定為這個節點
						}
						//如果舊節點的右兒子是自己節點的話
						else{
							if(thisNode->parent == NULL) currentNode->parent = NULL;	//避免對自己節點接到
							else currentNode->parent = thisNode->parent;
						}
						
						//如果舊節點的位置非最根處
						if(thisNode->parent != NULL){
							currentNode->parent = thisNode->parent;	//將這個節點與準備要被取代節點的父親節點連接
							
							//如果舊節點位於舊節點的父親節點的左兒子
							if(thisNode->parent->leftChild == thisNode){
								if(thisNode == currentNode) thisNode->parent->leftChild = NULL;	//如果要刪除的節點本身是樹葉的話
								else thisNode->parent->leftChild = currentNode;	//從舊節點的父親節點的左兒子指定為新節點
							}
							//如果舊節點位於舊節點的父親節點的右兒子
							else if(thisNode->parent->rightChild == thisNode){
								if(thisNode == currentNode) thisNode->parent->rightChild = NULL;	//如果要刪除的節點本身是樹葉的話
								else thisNode->parent->rightChild = currentNode;	//從舊節點的父親節點的右兒子指定為新節點
							}
							//如果都不是，則是程式內部出錯
							else return false;	//回傳失敗狀態
						}
						//如果節點是最根處
						else{
							if(currentNode->parent == NULL && currentNode->leftChild == NULL && currentNode->rightChild == NULL) firstNode = NULL;	//如果已經剩最後這個節點，則連firstNode連結也清除掉
							else{
								firstNode = currentNode;	//設定根節點為這個節點
								
							}
						}
						
						
						//釋放將被取代的節點
						BinarySearchTree_Node *deleteNode = thisNode;	//宣告指定即將被刪除的節點
						thisNode = currentNode;	//將新的節點取代舊節點
						delete deleteNode;	//刪除舊節點（釋放掉）
						//cout<<currentNode->getNumber()<<endl;	//DEBUG
						return true;	//回傳成功狀態
					}
				}
			}
			//如果沒搜尋到的話
			else return false;	//回傳失敗狀態
		}
		
		//印出節點資訊
		void printNodeInfo(int num){
			BinarySearchTree_Node *thisNode = searchNodeObject(num);	//尋找這個數的節點代入thisNode
			printObjectNodeInfo(thisNode);	//印出這個節點的相關資訊
		}
		
		//印出節點路徑
		void printSearchNode(int num){
			//如果根節點尚未建立的話
			if(firstNode == NULL){
				cout<<"No Node!!"<<endl;
			}
			//在已經有節點的情況下
			else{
				BinarySearchTree_Node *currentNode = firstNode;	//當前節點
				while(true){
					//若這個節點的數字比目前節點的數字大
					if(num > currentNode->number){
						//如果還能往右兒子找的話
						if(currentNode->rightChild != NULL){
							currentNode = currentNode->rightChild;	//節點指標往右兒子找
							cout<<"R -> ";
						}
						//如果已經是最底下兒子的話
						else{
							cout<<"No Found!"<<endl;
							break;
						}
					}
					//若這個節點的數字比目前節點的數字小
					else if(num < currentNode->number){
						//如果還能往左兒子找的話
						if(currentNode->leftChild != NULL){
							currentNode = currentNode->leftChild;	//節點指標往左兒子找
							cout<<"L -> ";
						}
						//如果已經是最底下兒子的話
						else{
							cout<<"No Found!"<<endl;
							break;
						}
					}
					//這個節點的數字與目前節點的數字相同
					else{
						cout<<"this"<<endl;	//DEBUG
						break;
					}
				}
			}
		}
		//印出Inorder清單
		void printInorderList(){
			if(firstNode == NULL) cout<<"isEmpty!!"<<endl;
			else{
				printInorderListHelper(firstNode,1);
				cout<<"end"<<endl;
			}
		}
		//印出Inorder節點資訊
		void printInorderInfo(){
			if(firstNode == NULL) cout<<"isEmpty!!"<<endl;
			else printInorderListHelper(firstNode,2);
		}
		//印出Preorder清單
		void printPreorderList(){
			if(firstNode == NULL) cout<<"isEmpty!!"<<endl;
			else{
				printPreorderListHelper(firstNode,1);
				cout<<"end"<<endl;
			}
		}
		//印出Preorder節點資訊
		void printPreorderInfo(){
			if(firstNode == NULL) cout<<"isEmpty!!"<<endl;
			else printPreorderListHelper(firstNode,2);
		}
};

int main(int argc, char **argv)
{
	BinarySearchTree tree;
	
	/*tree.insertNode(30);
	tree.insertNode(10);
	tree.insertNode(40);
	
	tree.printNodeInfo(30);
	tree.printNodeInfo(10);
	tree.printNodeInfo(40);
	                   
	tree.deleteNode(30);
	
	tree.printNodeInfo(30);
	tree.printNodeInfo(10);
	tree.printNodeInfo(40);*/
	int option;//選單選項
	while(option != 8){
		system("cls");
		cout<<"====選單===="<<endl;
		cout<<"1.新增"<<endl;
		cout<<"2.刪除"<<endl;
		cout<<"3.搜尋"<<endl;
		cout<<"4.Inorder列出"<<endl;
		cout<<"5.Inorder詳細列出"<<endl;
		cout<<"6.Preorder列出"<<endl;
		cout<<"7.Preorder詳細列出"<<endl;
		cout<<"8.離開"<<endl;
		cout<<"============"<<endl;
		cin >> option ;

		switch(option){
			case 1:	//1.新增
			{	
				int inputNumber;
				cout << "請輸入要新增的數字: "; 
				cin >> inputNumber;
				if(!tree.insertNode(inputNumber)){
					cout<<"你輸入的數字有重覆到喔！！"<<endl;
					system("pause");
				}
				break;
			}
			case 2:	//2.刪除
			{
				int inputNumber;
				
				cout << "請輸入要刪除的數字: "; 
				cin >> inputNumber;
				
				if(!tree.deleteNode(inputNumber)){
					cout<<"找不到你輸入的數字喔！！"<<endl;
					system("pause");
				}
				break;
			}
			case 3:	//3.搜尋 
			{		
				int inputNumber;
				
				cout << "請輸入要搜尋的數字: "; 
				cin >> inputNumber;
				
				tree.printNodeInfo(inputNumber);
				tree.printSearchNode(inputNumber);
				system("pause");
				break;
			}
			case 4:	//4.Inorder列出
			{
				tree.printInorderList();
				system("pause");
				break;
			}
			case 5:	//5.Inorder詳細列出
			{
				tree.printInorderInfo();
				system("pause");
				break;
			}
			case 6:	//6.Preorder列出
			{
				tree.printPreorderList();
				system("pause");
				break;
			}
			case 7:	//7.Preorder詳細列出
			{
				tree.printPreorderInfo();
				system("pause");
				break;
			}
		}
	}
	return 0;
}

